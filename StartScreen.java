package teamnorth.supermario;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceView;

/**
 * Created by Hunter on 5/27/2015.
 */
public class StartScreen {
    BitmapFactory.Options options = new BitmapFactory.Options();
    private final SurfaceView sv;
    private final Bitmap bitmap;

    public StartScreen(SurfaceView sv){

        bitmap = BitmapFactory.decodeResource(sv.getResources(),R.drawable.startscreen,options);
        this.sv=sv;

    }


    public void draw(Canvas c) {

        Rect dst = new Rect(0,0,sv.getWidth(),sv.getHeight());

        c.drawBitmap(bitmap, null, dst, null);

    }
}
