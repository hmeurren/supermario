package teamnorth.supermario;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceView;

import java.util.Random;

/**
 * Created by Hunter on 5/21/2015.
 */
public class Enemy implements TimeConscious {

    public  static final int   RADIUS      = 100;
    private       int velocity;
    private  int centerX;
    private       int centerY;
    private final Bitmap bitmap;
    private final SurfaceView sv;
    //private final Bitmap backdrop;

    public Enemy( SurfaceView sv ) {
        this.sv = sv;

        this.velocity     = 0;

        BitmapFactory.Options options = new BitmapFactory.Options();
      //  this.backdrop = BitmapFactory.decodeResource(sv.getResources(),
        //        R.drawable.background, options);
        Random rand= new Random();
        int  n = rand.nextInt(1);
     if (n==0){
        this.bitmap = BitmapFactory.decodeResource(sv.getResources(),
                R.drawable.goomba, options);
    }
        else{
            this.bitmap = BitmapFactory.decodeResource(sv.getResources(),
                    R.drawable.bulletbill, options);

        }
        this.centerY = sv.getWidth()/2 +100;
        if ( MarioSurfaceView.RIGHT_TO_LEFT ) {
            this.centerX =  sv.getWidth() -RADIUS;
        } else {
            this.centerX = sv.getWidth() -RADIUS;
        }
    }

    public int getCenterX() { return centerX; }
    public int getCenterY() { return centerY; }
    public int getRadius () { return RADIUS;  }

    @Override
    public void tick(Canvas canvas) {
        tickCenter();
        draw(canvas);
    }


    private void tickCenter() {
        this.centerX = this.centerX-25;
    }


    private void draw( Canvas c ) {
        Rect dst = new Rect( centerX-RADIUS, centerY-RADIUS,
                centerX+RADIUS, centerY+RADIUS);
//		c.rotate( 45 );
        c.drawBitmap(bitmap, null, dst, null);
//		c.rotate( -45 );
    }






}
