package teamnorth.supermario;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by Hunter on 5/19/2015.
 */
public class MarioSurfaceView extends SurfaceView
        implements SurfaceHolder.Callback, TimeConscious {

    private Background    background;
    private MarioRenderThread  renderThread;

    private Enemy                    Enemy;
    private Mario                    Mario;
    private StartScreen startscreen;
    public static final boolean RIGHT_TO_LEFT = false;
    private boolean start=true;


    public MarioSurfaceView(Context context) {
        super(context);
        // Notify the SurfaceHolder that you'd like to receive SurfaceHolder callbacks.
        getHolder().addCallback(this);

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        renderThread = new MarioRenderThread(this);
        renderThread.start();
        background = new Background(this);
          Enemy   = new  Enemy(this);
        Mario = new Mario(this);
        startscreen = new StartScreen(this);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        renderGame(canvas);
    }

    protected void renderGame(Canvas c) {
        c.clipRect(0, 0, getWidth(), getHeight());
        if(start == true)
        {
            startscreen.draw(c);

        }
        else {
            background.fillBackground(c);
            background.tick(c);
            Enemy.tick(c);
            Mario.tick(c);
        }
    }

    @Override
    public void tick(Canvas c) {
        renderGame(c);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {

        switch ( e.getAction() ) {
            case MotionEvent.ACTION_DOWN:
                if(start==true) {
                    start = false;
                }

                else {
                    Mario.jump();
                    break;
                }

            case MotionEvent.ACTION_UP:
                Mario.fall();
                break;
        }
        return true;
    }



}



