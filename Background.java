package teamnorth.supermario;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.SurfaceView;

/**
 * Created by Hunter on 5/18/2015.
 */
public class Background implements TimeConscious {
    protected final SurfaceView sv;
    protected final Bitmap bitmap;
    protected static final int BACKGROUND_COLOR = Color.WHITE;
    private int offsetX;
    public static final int OFFSET_X_INC  = 20;

    public Background( SurfaceView sv ) {
        this.sv = sv;
        //
        // Android creates a Drawable resource for any of these files when you save them in the
        // res/drawable/ directory.
        // In Java: R.drawable.filename
        // In XML: @[package:]drawable/filename
        //
        BitmapFactory.Options options = new BitmapFactory.Options();
//      options.inJustDecodeBounds = true;
//      options.inSampleSize = 4;
        bitmap =
                BitmapFactory.decodeResource( sv.getResources(),
                        R.drawable.background, options );
//      int    imageHeight = options.outHeight;
//      int    imageWidth  = options.outWidth;
//      String imageType   = options.outMimeType;
        offsetX  = 0;
    }

    public void fillBackground(Canvas c) {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(BACKGROUND_COLOR);
        c.drawPaint(paint);
    }

    private void draw(Canvas c) {
        Rect src  = new Rect( 0, 0, bitmap.getWidth(), bitmap.getHeight());
        Rect dst1 = new Rect(- sv.getWidth()+offsetX, 0,                 offsetX,   sv.getHeight());
        Rect dst2 = new Rect(                offsetX, 0,   sv.getWidth()+offsetX,   sv.getHeight());
        c.clipRect( 0, 0,   sv.getWidth(), sv.getHeight() );
        c.drawBitmap(bitmap, null, dst1, null);
        c.drawBitmap(bitmap, null, dst2, null);
    }


    @Override
    public void tick(Canvas canvas) {
        int w = sv.getWidth();
        if ( MarioSurfaceView.RIGHT_TO_LEFT ) {
            offsetX += OFFSET_X_INC;
            if ( offsetX > w ) {
                offsetX -= w;
            }
        } else {
            offsetX -= OFFSET_X_INC;
            if ( offsetX < 0 ) {
                offsetX += w;
            }
        }
        draw(canvas);
    }





}
