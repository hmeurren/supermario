package teamnorth.supermario;

/**
 * Created by Hunter on 5/18/2015.
 */
public class Point2D {

    public int x;
    public int y;

    public Point2D( int x, int y ) {
        this.x = x;
        this.y = y;
    }

    public Point2D( Point2D other ) {
        this.x = other.x;
        this.y = other.y;
    }
}
