package teamnorth.supermario;

import android.graphics.Canvas;

/**
* Created by Hunter on 5/19/2015.
        */
public interface TimeConscious{
        public void tick( Canvas canvas );
}
