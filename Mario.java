package teamnorth.supermario;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceView;

/**
 * Created by Hunter on 5/18/2015.
 */
public class Mario implements TimeConscious {

    public  static final int   THRUST      = 5;
    public  static final int   GRAVITATION = 5;
    public  static final float FRICTION    = 0.2f;
    public  static final int   RADIUS      = 100;

    private       int acceleration;
    private       int velocity;
    private final int centerX;
    private       int centerY;
    private final Bitmap bitmap;
    private final SurfaceView sv;
    private final Bitmap backdrop;
    private int counter = 0;

    public Mario( SurfaceView sv ) {
        this.sv = sv;
        this.acceleration = 0;
        this.velocity     = 0;

        BitmapFactory.Options options = new BitmapFactory.Options();
        this.backdrop = BitmapFactory.decodeResource(sv.getResources(),
                R.drawable.background, options);
        this.bitmap = BitmapFactory.decodeResource(sv.getResources(),
                R.drawable.papermario, options);
        this.centerY = sv.getWidth()/2 +100;
        if ( MarioSurfaceView.RIGHT_TO_LEFT ) {
            this.centerX = 2 * sv.getWidth() / 3;
        } else {
            this.centerX = 1 * sv.getWidth() / 3;
        }
    }



    public int getCenterX() { return centerX; }
    public int getCenterY() { return centerY; }
    public int getRadius () { return RADIUS;  }

    /*public void setFalling() {
        System.out.print(this.getCenterY());
         System.out.print(backdrop.getHeight());
             if ( centerY < sv.getHeight()/2 + RADIUS ) {

                 velocity = 0;




        } else {
            this.acceleration = GRAVITATION; // Falling
        }
    }
    public void setRising() {
        if (centerY > sv.getHeight()/2+2*RADIUS) {
            velocity = 0;
            this.acceleration= 0;
        } else {
            this.acceleration = -THRUST; // Rising
        }
    }*/

    public void jump(){
        if (this.getCenterY()>(sv.getHeight()/2)){
            velocity=0;
        }
        else{
            counter++;
            velocity=30;
        }


    }

    public void fall(){

        if(this.getCenterY()==(sv.getHeight()/2+RADIUS)){
        velocity=0;


        }
        else{
            velocity=30;
        }


    }

    private void tickCenter() {
        if (1<= counter && counter< 6){
            centerY-=velocity;
            counter++;
        }
        if (6<= counter && counter< 11){
            centerY+=velocity ;
            counter++;
            if (counter==11){
                counter=0;
            }

        }



        if ( centerY > sv.getHeight() - RADIUS ) {
            centerY = sv.getHeight() - RADIUS;
            velocity = 0;
        } else if ( centerY < RADIUS ) {
            centerY  = RADIUS;
            velocity = 0;
        }
    }

    @Override
    public void tick(Canvas canvas) {
        tickCenter();
        draw(canvas);
    }


    private void draw( Canvas c ) {
        Rect dst = new Rect( centerX-RADIUS, centerY-RADIUS,
                centerX+RADIUS, centerY+RADIUS);
//		c.rotate( 45 );
        c.drawBitmap(bitmap, null, dst, null);
//		c.rotate( -45 );
    }



}
